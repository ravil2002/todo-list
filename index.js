const express = require('express');
const mongoose = require('mongoose')
const exphbp = require('express-handlebars')
const todoRoutes = require('./routes/todos')
const path = require('path')
const app = express()
const hbs = exphbp.create({
    defaultLayout: 'main',
    extname: 'hbs'
})

app.engine('hbs', hbs.engine)
app.set('view engine', 'hbs')
app.set('views', 'views')
app.use(express.urlencoded({    extended: true}))
app.use(express.static(path.join(__dirname, 'public')))
app.use(todoRoutes)


const PORT = process.env.PORT || 3000

async function start () {
    try {
        await mongoose.connect(
        'mongodb+srv://ravil:Ravilka6142@cluster0-juz2u.mongodb.net/test', 
        {
            useNewUrlParser: true,
            useFindAndModify: false
        })
    }
    catch(err) {
        console.log(err)
    }
    app.listen(3000, () => {
        console.log('Server has been started!')
    })
}



start()